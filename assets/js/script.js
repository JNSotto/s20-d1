/*arrow functions*/

/*function sampleFunction(){
	alert(`Hello, I am filipino.`); 
}; 

sampleFunction(); 

const sampleFunction1 = () => {
	alert(`Hello, I am an Arrow Function`)
}
sampleFunction1();*/

//Implicit return with arrow function

function prodNum(num1, num2){
	return num1 * num2
}
let prod = prodNum(25,25);
console.log(prod);

const prodNum1 = (num1,num2) => num1 * num2; // arrow function with implicit return 
let prod1 = prodNum1(25,10);
console.log(prod1); 

const multiplyNum = (num1,num2) => {
	return num1 * num2;
};
let prod2 = multiplyNum(5,10);
console.log(prod2);

/*function ans(a){
	if (a < 10){
		return 'true';
	} 
	else {
		return 'false';
	}
};

let answer = ans(5);
console.log(answer);
*/


//"ternary operator" '?:' using arrow function 
//syntax: condition ? expression1 : expression2;

/*const ans = (a) => (a < 10) ? 'true' : 'false';
let answer = ans(5);
console.log(answer);

let mark = prompt('Enter your grade: ');// user input value 

let grade = (mark >= 75) ? 'Pass' : 'Fail';
console.log(`You ${grade} the exam.`);*/

/*
	Mini-Activity 
	
		convert the following function into an arrow function and display the result in console.
	
 */
/*		let number = [1,2,3,4,5];

		Let allValid = number.every(function(number){
			return (number < 3);
		});
		
		Let filterValid = number.filter(function(number){
			return (number < 3);
		});

		let numberMap = numbers.map(function(number){
			return number * number
		});*/

let number = [1,2,3,4,5];

let allValid = number.every(number => number < 3);
let filterValid = number.filter(number => number < 3);
let numberMap = number.map(number => number * number);

console.log(allValid);
console.log(filterValid);
console.log(numberMap);

// to check if number is positive, negative or zero
let num = 0;
let result = (num >= 0) ? (num == 0 ? 'zero' : 'positive') : 'negative';
console.log(`The number is ${result}`);

/*=================================================================================*/

//JSON - JavaScript Object Notation 
	// JSON is a string but formatted as JS Object
	// JSON is popularly used to pass data from one application to another
	// JSON is not only in JS bit also in other programming Language to pass data 
	// this is why it is specified as JavaScript Object Notation 
	// file extension .json
	// There is a way to turn JSON as JS Objects and there is way to turn JS Objects to JSON 

//JS Object are not the same as JSON 
	// JSON is a string 
	// JS Object is an object
	// JSON keys are surrounded with double qoutes.
	
	//Syntax:Reflect.get(target, propertyKey, receiver);
	/*
		{ 
			"key1" : "value1",
			"key2" : "value2"
		}
	 
	
	Supported:
	-string
	-object
	-array
	-boolean
	-null
	 */


const person = {
	"name" : "Juan",
	"weight" : 175,
	"age" : 20,
	"eyeColor" : "brown",
	"cars" : ['toyota', 'honda'],
	"favoriteBooks" : {
		"title" : "When the fire Nation Attack",
		"author" : "Nickeloden",
		"release" : "2021"

	}
};

console.log(typeof(person));

/*
	Mini-Activity 

		Create an array of JSON format data with atleast 2 items
		Name the array as assets 
		Each JSON format should have the following key-value pairs:

			id-<valve>
			name -<value>
			description - <value>
			isAvailable - <value>
			dateAdded - <value>

 */

/*let assets = [{
	"id" : "ES3560GHT23",
	"name" : "Bear Brand",
	"description" : "Powder Milk",
	"isAvailable" : true,
	"dateAdded" :"10-21-21"
},

{
	"id" : "EF356HHT43",
	"name" : "MILO",
	"description" : "Chocolate Powder Milk",
	"isAvailable" : false,
	"dateAdded" :"09-03-20"
}
]

console.log(assets);*/

// XML format - extensible markup language
/*
	<note>
		<to>Juan</to>
		<from>Maria Clara</from>
		<heading>Reminder</heading>
		<body>Don't forget me this weekend!</body>
	<note>
 */
// JSON - key value pair - string 
/*{
	"to" : "Juan",
	"from" : "Maria Clara",
	"heading" : "Reminder",
	"body" : "Don't forget this weekend!",
}
*/
//Solution
/*let assets = [
	{
	"id" : "item-1",
	"name" : "Construction Crane",
	"description" : "Doesn't actually fly",
	"isAvailable" : true,
	"dateAdded" :"July 7, 2018"
	},
	{
	"id" : "item-2",
	"name" : "Backhoe",
	"description" : "It has claw",
	"isAvailable" : true,
	"dateAdded" :"July 7, 2018"
	}
];
*/


//Stringify - method to convert JavaScript Object to JSON and vice versa
	//converting JS Objects into JSON.
		//this is commonly used when trying to pass data from one application to another via the use HTTP Request.
		//HTTP requests are requests from resource between server and a client(browser).
		//JSON format is also in database. 
		
const cat = {
	"name":"Mashiro",
	"age": 3,
	"weight": 20
}

console.log (cat);

const catJSON = JSON.stringify(cat);
console.log(catJSON);

//parse - reverse
const json = '{"name":"Mashiro","age":3,"weight":20}'; // use backpick `` or single quotation '', not double quotation "" in parse.
const cat1 = JSON.parse(json);
console.log(cat1);

let batchesArr = [
	{
		batchName : "Batch 131"
	},
	{
		batchName : "Batch 132"
	}
];

console.log(JSON.stringify(batchesArr));

let batchesJSON = `[
	{
		"batchName":"Batch 131"
	},
	{
		"batchName":"Batch 132"
	}
]`;

console.log(JSON.parse(batchesJSON));

